import javax.swing.SwingUtilities;
import view.MainFrame;

public class Start {

  /**
   * Start point of program.
   */
  public static void main(String[] args) {
    SwingUtilities.invokeLater(MainFrame::new);
  }
}
