package logic;

import java.awt.Graphics;

/**
 * Class that display work of cell which will be painted for user.
 */
public abstract class Cell extends Item {

  /**
   * Draw method. For drawing cells.
   */
  public abstract void draw(Graphics g);
}
