package logic;

/**
 * Class that display work of character.
 */
public class Character extends Item implements Moveable {

  /**
   * Table in which character will be running.
   */
  private Table table;
  /**
   * Number of steps that is needed to complete level.
   */
  public static int steps = 0;

  @Override
  public void move(Table in) {
    this.table = in;
  }

  @Override
  public void left() {
    Item item = table.getByCoordinates(getxCoordinate() - 1, getyCoordinate());
    if (item instanceof Floor) {
      if (!(((Floor) item).isHaveCrate())) {
        ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
            .setHaveCharacter(false);
        ((Floor) table.getByCoordinates(getxCoordinate() - 1, getyCoordinate()))
            .setHaveCharacter(true);
        setxCoordinate(getxCoordinate() - 1);
        steps++;
        Bridge.doAction(steps);
      } else {
        Item newItem = table
            .getByCoordinates(getxCoordinate() - 2, getyCoordinate());
        if (newItem instanceof Floor) {
          if (!((Floor) newItem).isHaveCrate()) {
            ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
                .setHaveCharacter(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate() - 1, getyCoordinate()))
                .setHaveCharacter(true);

            ((Floor) table
                .getByCoordinates(getxCoordinate() - 1, getyCoordinate()))
                .setHaveCrate(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate() - 2, getyCoordinate()))
                .setHaveCrate(true);

            setxCoordinate(getxCoordinate() - 1);
            steps++;
            Bridge.doAction(steps);
          }
        }
      }
    }
  }

  @Override
  public void right() {
    Item item = table.getByCoordinates(getxCoordinate() + 1, getyCoordinate());
    if (item instanceof Floor) {
      if (!((Floor) item).isHaveCrate()) {
        ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
            .setHaveCharacter(false);
        ((Floor) table.getByCoordinates(getxCoordinate() + 1, getyCoordinate()))
            .setHaveCharacter(true);
        setxCoordinate(getxCoordinate() + 1);
        steps++;
        Bridge.doAction(steps);
      } else {
        Item newItem = table
            .getByCoordinates(getxCoordinate() + 2, getyCoordinate());
        if (newItem instanceof Floor) {
          if (!((Floor) newItem).isHaveCrate()) {
            ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
                .setHaveCharacter(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate() + 1, getyCoordinate()))
                .setHaveCharacter(true);

            ((Floor) table
                .getByCoordinates(getxCoordinate() + 1, getyCoordinate()))
                .setHaveCrate(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate() + 2, getyCoordinate()))
                .setHaveCrate(true);

            setxCoordinate(getxCoordinate() + 1);

            steps++;
            Bridge.doAction(steps);
          }
        }
      }
    }

  }

  @Override
  public void up() {
    Item item = table.getByCoordinates(getxCoordinate(), getyCoordinate() - 1);
    if (item instanceof Floor) {
      if (!((Floor) item).isHaveCrate()) {

        ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
            .setHaveCharacter(false);
        ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate() - 1))
            .setHaveCharacter(true);

        setyCoordinate(getyCoordinate() - 1);

        steps++;
        Bridge.doAction(steps);
      } else {
        Item newItem = table
            .getByCoordinates(getxCoordinate(), getyCoordinate() - 2);
        if (newItem instanceof Floor) {
          if (!((Floor) newItem).isHaveCrate()) {

            ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
                .setHaveCharacter(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate(), getyCoordinate() - 1))
                .setHaveCharacter(true);

            ((Floor) table
                .getByCoordinates(getxCoordinate(), getyCoordinate() - 1))
                .setHaveCrate(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate(), getyCoordinate() - 2))
                .setHaveCrate(true);

            setyCoordinate(getyCoordinate() - 1);

            steps++;
            Bridge.doAction(steps);
          }
        }
      }
    }
  }

  @Override
  public void down() {

    Item item = table.getByCoordinates(getxCoordinate(), getyCoordinate() + 1);
    if (item instanceof Floor) {
      if (!((Floor) item).isHaveCrate()) {

        ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
            .setHaveCharacter(false);
        ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate() + 1))
            .setHaveCharacter(true);
        setyCoordinate(getyCoordinate() + 1);

        steps++;
        Bridge.doAction(steps);
      } else {
        Item newItem = table
            .getByCoordinates(getxCoordinate(), getyCoordinate() + 2);
        if (newItem instanceof Floor) {
          if (!((Floor) newItem).isHaveCrate()) {

            ((Floor) table.getByCoordinates(getxCoordinate(), getyCoordinate()))
                .setHaveCharacter(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate(), getyCoordinate() + 1))
                .setHaveCharacter(true);

            ((Floor) table
                .getByCoordinates(getxCoordinate(), getyCoordinate() + 1))
                .setHaveCrate(false);
            ((Floor) table
                .getByCoordinates(getxCoordinate(), getyCoordinate() + 2))
                .setHaveCrate(true);

            setyCoordinate(getyCoordinate() + 1);

            steps++;
            Bridge.doAction(steps);
          }
        }
      }
    }
  }

}
