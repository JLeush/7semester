package logic;

/**
 * Interface that makes target Item moveable.
 */
public interface Moveable {

  /**
   * Register object and shows him table in which it will be running.
   *
   * @param in - current Table.
   */
  void move(Table in);

  /**
   * When object is moves to the left.
   */
  void left();

  /**
   * When object is moves to the right.
   */
  void right();

  /**
   * When object is moves up.
   */
  void up();

  /**
   * When object is moves down.
   */
  void down();
}
