package logic;

import java.awt.Graphics;

/**
 * A class that displays the work of a cell without a wall.
 */
public class Floor extends Cell {

  private final int SIDE = 25;

  /**
   * Variable that shows whether cell contain diamond.
   */
  private boolean haveDiamond;
  /**
   * Variable that shows whether character is in target cell.
   */
  private boolean haveCharacter;
  /**
   * Variable that shows whether crate is in target cell.
   */
  private boolean haveCrate;

  /**
   * Constructor for cell without any objects.
   *
   * @param x - vertical coordinate.
   * @param y- horizontal coordinate.
   */
  Floor(int x, int y) {
    setxCoordinate(x);
    setyCoordinate(y);
  }

  /**
   * Constructor for cell with diamond.
   *
   * @param x - vertical coordinate.
   * @param y- horizontal coordinate.
   * @param haveDiamond - for diamond.
   */
  Floor(int x, int y, boolean haveDiamond) {
    setxCoordinate(x);
    setyCoordinate(y);
    this.haveDiamond = haveDiamond;
  }

  /**
   * Constructor for cell with crate.
   *
   * @param x - vertical coordinate.
   * @param y- horizontal coordinate.
   * @param haveDiamond - for diamond.
   * @param haveCrate - for crate.
   */
  Floor(int x, int y, boolean haveDiamond, boolean haveCrate) {
    setxCoordinate(x);
    setyCoordinate(y);
    this.haveDiamond = haveDiamond;
    this.haveCrate = haveCrate;
  }

  /**
   * Constructor for cell with character.
   *
   * @param x - vertical coordinate.
   * @param y- horizontal coordinate.
   * @param haveDiamond - for diamond.
   * @param haveCrate - for crate.
   * @param haveCharacter - for character.
   */
  Floor(int x, int y, boolean haveDiamond, boolean haveCrate,
      boolean haveCharacter) {
    setxCoordinate(x);
    setyCoordinate(y);
    this.haveDiamond = haveDiamond;
    this.haveCharacter = haveCharacter;
    this.haveCrate = haveCrate;
  }

  /**
   * Getter for haveDiamond.
   *
   * @return haveDiamond.
   */
  public boolean isHaveDiamond() {
    return haveDiamond;
  }

  /**
   * Setter for haveDiamond.
   *
   * @param haveDiamond - new state.
   */
  public void setHaveDiamond(boolean haveDiamond) {
    this.haveDiamond = haveDiamond;
  }

  /**
   * Getter for haveCharacter.
   *
   * @return haveCharacter.
   */
  public boolean isHaveCharacter() {
    return haveCharacter;
  }

  /**
   * Setter for haveCharacter.
   *
   * @param haveCharacter - new state.
   */
  public void setHaveCharacter(boolean haveCharacter) {
    this.haveCharacter = haveCharacter;
  }

  /**
   * Getter for haveCrate.
   *
   * @return haveCrate.
   */
  public boolean isHaveCrate() {
    return haveCrate;
  }

  /**
   * Setter for haveCrate.
   *
   * @param haveCrate - new state.
   */
  public void setHaveCrate(boolean haveCrate) {
    this.haveCrate = haveCrate;
  }

  /**
   * Draw method for cell.
   */
  @Override
  public void draw(Graphics g) {
    if (haveDiamond && haveCrate) {
      g.drawImage(utilities.getImage("tnt"), getxCoordinate() * SIDE,getyCoordinate() * SIDE, SIDE, SIDE, null);
    } else if (haveCharacter) {
      g.drawImage(utilities.getImage("character"), getxCoordinate() * SIDE,getyCoordinate() * SIDE, SIDE, SIDE, null);
    } else if (haveDiamond) {
      g.drawImage(utilities.getImage("diamond"), getxCoordinate() * SIDE,getyCoordinate() * SIDE, SIDE, SIDE, null);
    } else if (haveCrate) {
      g.drawImage(utilities.getImage("crate"), getxCoordinate() * SIDE,getyCoordinate() * SIDE, SIDE, SIDE, null);
    } else {
      g.drawImage(utilities.getImage("land"), getxCoordinate() * SIDE,getyCoordinate() * SIDE, SIDE, SIDE, null);
    }
  }
}
