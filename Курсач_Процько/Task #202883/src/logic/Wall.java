package logic;

import java.awt.Graphics;

/**
 * Class that display work of a cell that contain wall.
 */
public class Wall extends Cell {

  private final int SIDE = 25;

  /**
   * Constructor for wall.
   *
   * @param x - vertical coordinate.
   * @param y- horizontal coordinate.
   */
  public Wall(int x, int y) {
    setxCoordinate(x);
    setyCoordinate(y);
  }

  /**
   * Draw method for wall.
   */
  @Override
  public void draw(Graphics g) {
    g.drawImage(utilities.getImage("wall"), getxCoordinate() * SIDE,
        getyCoordinate() * SIDE, SIDE, SIDE, null);
  }
}
