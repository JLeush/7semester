package logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Configuration class that load collection of cells from files.
 */
public class Level {

  /**
   * Variable that contain the current column number when the Cell is written to
   * the collection.
   */
  public static int columnNumber = 0;
  /**
   * Variable that contain the current row number when the Cell is written to
   * the collection.
   */
  public static int rowNumber = 0;

  /**
   * Method that loads Collections of cells from files.
   *
   * @param path - path of target file.
   * @return collection of cells.
   */
  public static List<Cell> loadLevel(String path) {
    List<Cell> list = new ArrayList<>();
    File file = new File(path);
    try (FileInputStream fileInputStream = new FileInputStream(file)) {
      Scanner scanner = new Scanner(fileInputStream, "UTF-8");
      rowNumber = 0;
      while (scanner.hasNext()) {
        String row = scanner.nextLine();
        columnNumber = 0;

        Stream.of(row).flatMap(e -> Stream.of(e.split(""))).forEach(c -> {
          if (c.equals("W")) {
            list.add(new Wall(columnNumber, rowNumber));
          } else if (c.equals("F")) {
            list.add(new Floor(columnNumber, rowNumber));
          } else if (c.equals("D")) {
            list.add(new Floor(columnNumber, rowNumber, true));
          } else if (c.equals("C")) {
            list.add(new Floor(columnNumber, rowNumber, false, true));
          } else if (c.equals("S")) {
            list.add(new Floor(columnNumber, rowNumber, false, false, true));
          }else {
            Logger.getAnonymousLogger().log(java.util.logging.Level.INFO, "Incorrect map components");
          }
          columnNumber++;
        });
        rowNumber++;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return list;
  }

}
