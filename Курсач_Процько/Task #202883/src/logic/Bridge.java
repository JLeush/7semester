package logic;

import view.MainFrame;

/**
 * Class that connects JLabel to print number of steps to a step counter. This
 * class also checks the completion of the level.
 */
public class Bridge {

  /**
   * Variable that contain MainFrame object.
   */
  private static MainFrame mainFrame;
  /**
   * Variable that contain the Bridge state. True if mainFrame is connected,
   * false if there is no connection.
   */
  private static boolean isSet = false;

  /**
   * Private constructor for class. To prevent creating an instance of the
   * class.
   */
  private Bridge() {
  }

  /**
   * Setter for MainFrame.
   *
   * @param mf instance of MainFrame.
   */
  public static void setObserver(MainFrame mf) {
    mainFrame = mf;
    isSet = true;
  }

  /**
   * Method that reset text in target JLabel on changing steps number.
   *
   * @param steps - number of steps.
   */
  static void doAction(int steps) {
    if (isSet) {
      if (steps != 1) {
        mainFrame.getCounter().setText(steps + " steps");
      } else {
        mainFrame.getCounter().setText(steps + " step");
      }

      if (isCompleted()) {
        mainFrame.getCounter()
            .setText("You completed level in " + steps + " steps");
      }
    }
  }


  /**
   * Method that check the completion of the level.
   *
   * @return true if level is complete, false if don`t completed.
   */
  private static boolean isCompleted() {
    final boolean[] finished = {true};
    mainFrame.getGamePanel().getTable().getCurrentTable().forEach(e -> {
      if (e instanceof Floor) {
        if (((Floor) e).isHaveDiamond() && !((Floor) e).isHaveCrate()) {
          finished[0] = false;
        }
      }
    });
    return finished[0];
  }


}
