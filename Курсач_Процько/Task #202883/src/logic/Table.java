package logic;

import java.awt.Graphics;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that display work of table of cells.
 */
public class Table {

  /**
   * Collection that contain all cells of game.
   */
  private List<Cell> currentTable;
  /**
   * Variable that contain instance of Character.
   */
  private Character character;

  /**
   * Default constructor in which instance of the Character is generated
   */
  public Table() {
    character = new Character();
  }

  /**
   * Getter for character.
   */
  public Character getCharacter() {
    return character;
  }

  /**
   * Getter for collection of cells.
   */
  public List<Cell> getCurrentTable() {
    return currentTable;
  }

  /**
   * Method that set outside collection to current collection in table. And also
   * get coordinates of character.
   */
  public void setCurrentTable(List<Cell> currentTable) {
    boolean isChar = false;
    this.currentTable = currentTable;
    for (Cell cell : currentTable) {
      if (cell instanceof Floor) {
        if (((Floor) cell).isHaveCharacter()) {
          isChar = true;
          character.setxCoordinate(cell.getxCoordinate());
          character.setyCoordinate(cell.getyCoordinate());
        }
      }
    }
    if(!isChar){
      Logger.getAnonymousLogger().log(Level.INFO, "There no character on map");
    }
  }


  /**
   * Method that get Item object for their coordinates.
   *
   * @param x - vertical coordinate.
   * @param y- horizontal coordinate.
   * @return the Item if it is contained in the collection or null.
   */
  public Item getByCoordinates(int x, int y) {
    Cell target = null;
    for (Cell aCurrentTable : currentTable) {
      if (aCurrentTable.getxCoordinate() == x
          && aCurrentTable.getyCoordinate() == y) {
        target = aCurrentTable;
        break;
      }
    }
    if (target == null) {
      Logger.getAnonymousLogger()
          .log(Level.WARNING, "Target cell was not found");
    }

    return target;
  }

  /**
   * Draw method for table.
   */
  public void drawTable(Graphics g) {
    currentTable.forEach(e -> e.draw(g));
  }

}
