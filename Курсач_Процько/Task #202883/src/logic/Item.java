package logic;

import view.ImageUtilities;

/**
 * Class that display work of all objects in game that can have coordinates.
 */
public abstract class Item {

  /**
   * Instance of ImageUtilities class that is responsible for getting item`s
   * image to draw them.
   */
  ImageUtilities utilities = ImageUtilities.getInstance();
  /**
   * Variable that contain vertical coordinate of Item.
   */
  private int xCoordinate;
  /**
   * Variable that contain horizontal coordinate of Item.
   */
  private int yCoordinate;


  /**
   * Getter for vertical coordinate.
   */
  public int getxCoordinate() {
    return xCoordinate;
  }

  /**
   * Setter for vertical coordinate.
   */
  public void setxCoordinate(int xCoordinate) {
    this.xCoordinate = xCoordinate;
  }

  /**
   * Getter for horizontal coordinate.
   */
  public int getyCoordinate() {
    return yCoordinate;
  }

  /**
   * Setter for horizontal coordinate.
   */
  public void setyCoordinate(int yCoordinate) {
    this.yCoordinate = yCoordinate;
  }

}
