package view;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import logic.Table;

/**
 * Class that display table of cells to user.
 */
public class GamePanel extends JPanel {

  /**
   * Current table.
   */
  private Table table;

  /**
   * Constructor for GamePanel.
   *
   * @param table - starting table.
   */
  public GamePanel(Table table){
    this.table = table;
    table.getCharacter().move(table);
    addMouseListener(new MyMouseListener());
    setFocusable(true);
    /*addKeyListener(new MyKeyListener());*/
  }

  /**
   * Getter for table.
   */
  public Table getTable() {
    return table;
  }

  /**
   * Method that draws table.
   */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    table.drawTable(g);
  }

  /**
   * Inner class that check mouse clicking and do moves when user play game.
   */
  class MyMouseListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      super.mouseClicked(e);
      int x = e.getX();
      int y = e.getY();

      int chX = table.getCharacter().getxCoordinate() * 25;
      int chY = table.getCharacter().getyCoordinate() * 25;

      if (x > (chX) && x < (chX + 25) && y > (chY - 25) && y < chY) {
        table.getCharacter().up();
        repaint();
      } else if (x > chX && x < (chX + 25) && y > (chY + 25) && y < (chY
          + 50)) {
        table.getCharacter().down();
        repaint();
      } else if (x > (chX - 25) && x < chX && y > chY && y < (chY + 25)) {
        table.getCharacter().left();
        repaint();
      } else if (x > (chX + 25) && x < (chX + 50) && y > chY && y < (chY
          + 25)) {
        table.getCharacter().right();
        repaint();
      }
    }
  }
}
