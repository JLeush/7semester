package view;

import java.awt.Choice;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import logic.Bridge;
import logic.Character;
import logic.Level;
import logic.Table;

/**
 * Main Frame of the game.
 */
public class MainFrame extends JFrame {

  /**
   * Variable that contain align retreat from top and left.
   */
  private static final int ALIGN = 13;
  /**
   * Variable that contain height of the fields.
   */
  private final int FIELD_HEIGHT = 27;
  /**
   * Variable that contain width of the buttons.
   */
  private final int BUTTON_WIDTH = 100;

  /**
   * Variable that contain width of the frame.
   */
  private final int frameWidth = 343;
  /**
   * Variable that contain height of the frame.
   */
  private final int frameHeight = 481;

  /**
   * Variable that contain GamePanel instance.
   */
  private GamePanel gamePanel;
  /**
   * Variable that contain current table.
   */
  private Table table;
  /**
   * Label which shows number of steps.
   */
  private JLabel counter;
  /**
   * Button that opens previous level of game.
   */
  private JButton previousMap;
  /**
   * Button that restart current level.
   */
  private JButton restartMap;
  /**
   * Button which open next level of game.
   */
  private JButton nextMap;
  /**
   * Button that loads level from file.
   */
  private JButton loadMap;
  /**
   * Choice that allow user to chose level of game.
   */
  private Choice levelChoice;
  /**
   * Label that contain name of level or loaded file.
   */
  private JLabel title;
  /**
   * Label for word 'Level'.
   */
  private JLabel wordLevel;
  /**
   * Variable that contain current level.
   */
  private int level = 1;
  /**
   * Variable that contain current path file with map.
   */
  private String currentPath;

  /**
   * Variable that contain current number of rows in table.
   */
  private int rows;
  /**
   * Variable that contain current number of columns in table.
   */
  private int columns;

  /**
   * Constructor for Frame.
   */
  public MainFrame() {
    initFrame();
    initComponents();
    setVisible(true);
    setFocusable(true);
    Bridge.setObserver(this);
    java.util.logging.Logger.getAnonymousLogger()
        .log(java.util.logging.Level.INFO, "The program is running");
  }

  /**
   * Setting all properties that belong to the main frame.
   */
  private void initFrame() {
    setSize(new Dimension(frameWidth, frameHeight));
    setMinimumSize(new Dimension(frameWidth, frameHeight));
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setLayout(null);
    setTitle("Sokoban");

  }

  /**
   * This method convert simple text in html box.
   *
   * @param str - target string.
   * @param size - font size.
   * @return modified string.
   */
  private String setTitleText(String str, int size) {
    return "<html><div style='font-size:" + size + "px;'>" + str
        + "</div></html>";
  }

  /**
   * Setting all properties that belong to the components.
   */
  private void initComponents() {

    title = new JLabel(setTitleText("1 level", 18));
    table = new Table();
    try {
      currentPath = getClass().getClassLoader().getResource(
          "resources/levels/Level_1.txt").getPath();
      table.setCurrentTable(Level.loadLevel(currentPath));
      gamePanel = new GamePanel(table);
    } catch (NullPointerException npe) {
      java.util.logging.Logger.getAnonymousLogger()
          .log(java.util.logging.Level.WARNING, "Start map missing");
    }

    addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_UP) {
          table.getCharacter().up();
          gamePanel.repaint();
        } else if (code == KeyEvent.VK_DOWN) {
          table.getCharacter().down();
          gamePanel.repaint();
        } else if (code == KeyEvent.VK_LEFT) {
          table.getCharacter().left();
          gamePanel.repaint();
        } else if (code == KeyEvent.VK_RIGHT) {
          table.getCharacter().right();
          gamePanel.repaint();
        }
      }
    });

    counter = new JLabel("", SwingConstants.RIGHT);
    previousMap = new JButton("Previous");
    restartMap = new JButton("Restart");
    nextMap = new JButton("Next");
    loadMap = new JButton("Load map");
    levelChoice = new Choice();
    wordLevel = new JLabel("Level");

    setBounds();
    addComponents();
    addListeners();

  }

  /**
   * Setting location in frame for components.
   */
  private void setBounds() {
    title.setBounds(ALIGN, ALIGN / 2, 100, FIELD_HEIGHT);
    rows = Level.rowNumber * 25;
    columns = Level.columnNumber * 25;

    gamePanel.setBounds(ALIGN, ALIGN + 30, columns, rows);
    counter.setBounds(columns - 200 + ALIGN, ALIGN / 2, 200, FIELD_HEIGHT);
    previousMap.setBounds(ALIGN, ALIGN * 4 + rows, BUTTON_WIDTH, FIELD_HEIGHT);
    restartMap.setBounds(ALIGN + BUTTON_WIDTH, ALIGN * 4 + rows, BUTTON_WIDTH,
        FIELD_HEIGHT);
    nextMap.setBounds(ALIGN + BUTTON_WIDTH * 2, ALIGN * 4 + rows, BUTTON_WIDTH,
        FIELD_HEIGHT);
    loadMap.setBounds(ALIGN + BUTTON_WIDTH, ALIGN * 5 + 40 + rows, BUTTON_WIDTH,
        FIELD_HEIGHT);
    wordLevel
        .setBounds(ALIGN + 200 + 10, ALIGN * 5 + 40 + rows, 50, FIELD_HEIGHT);
    levelChoice
        .setBounds(ALIGN + BUTTON_WIDTH * 2 + 10 + 50, ALIGN * 5 + 40 + rows,
            40, FIELD_HEIGHT);
  }

  /**
   * Adding components to main frame.
   */
  private void addComponents() {
    levelChoice.add(" ");
    levelChoice.add(String.valueOf(new Integer(1)));
    levelChoice.add(String.valueOf(new Integer(2)));
    levelChoice.add(String.valueOf(new Integer(3)));
    levelChoice.add(String.valueOf(new Integer(4)));
    levelChoice.add(String.valueOf(new Integer(5)));

    add(title);
    add(counter);
    add(gamePanel);
    add(previousMap);
    add(restartMap);
    add(nextMap);
    add(loadMap);
    add(wordLevel);
    add(levelChoice);
  }

  /**
   * Adding listeners for components.
   */
  private void addListeners() {

    restartMap.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        Character.steps = 0;
        counter.setText("");
        table.setCurrentTable(Level.loadLevel(currentPath));
        gamePanel.updateUI();
        requestFocus(false);
      }
    });

    previousMap.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (level > 1) {
          Character.steps = 0;
          counter.setText("");
          level--;
          try {
            currentPath = getClass().getClassLoader().getResource(
                "resources/levels/Level_" + level + ".txt").getPath();
            table.setCurrentTable(Level.loadLevel(currentPath));
            title.setText(setTitleText(level + " level", 18));
            gamePanel.updateUI();
          } catch (NullPointerException npe) {
            Logger.getAnonymousLogger()
                .log(java.util.logging.Level.WARNING, "Level missing");
          }
        }
        requestFocus(false);
      }
    });

    nextMap.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (level < 5 && level != 0) {
          Character.steps = 0;
          counter.setText("");
          level++;
          try {
            currentPath = getClass().getClassLoader().getResource(
                "resources/levels/Level_" + level + ".txt").getPath();
            table.setCurrentTable(Level.loadLevel(currentPath));
            title.setText(setTitleText(level + " level", 18));
            gamePanel.updateUI();
          } catch (NullPointerException npe) {
            Logger.getAnonymousLogger()
                .log(java.util.logging.Level.WARNING, "Level missing");
          }
        }
        requestFocus(false);
      }
    });

    loadMap.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        Character.steps = 0;
        counter.setText("");
        counter.setAlignmentX(Component.RIGHT_ALIGNMENT);
        try {
          FileDialog fileDialog = new FileDialog(MainFrame.this, "Load level",
              FileDialog.LOAD);
          fileDialog.setFile("*.txt;*.map");
          fileDialog.setVisible(true);
          title.setText(setTitleText(fileDialog.getFile()
              .substring(0, fileDialog.getFile().lastIndexOf(".")), 18));
          level = 0;
          currentPath = fileDialog.getDirectory() + fileDialog.getFile();
          table.setCurrentTable(Level.loadLevel(currentPath));
          resize();
          gamePanel.updateUI();
        } catch (NullPointerException npe) {
          java.util.logging.Logger.getAnonymousLogger()
              .log(java.util.logging.Level.WARNING, "Incorrect path");
        }
        requestFocus(false);
      }
    });

    levelChoice.addItemListener(e -> {
      if (!levelChoice.getSelectedItem().equals(" ")) {
        Character.steps = 0;
        counter.setText("");
        try {
          level = Integer.parseInt(levelChoice.getSelectedItem());
          currentPath = getClass().getClassLoader().getResource(
              "resources/levels/Level_" + level + ".txt").getPath();
          table.setCurrentTable(Level.loadLevel(currentPath));
          title.setText(setTitleText(level + " level", 18));
          setDefaultSize();
          gamePanel.updateUI();
        } catch (NullPointerException npe) {
          java.util.logging.Logger.getAnonymousLogger()
              .log(java.util.logging.Level.WARNING, "Map missing");
        }
      }
      requestFocus(false);
    });

  }

  /**
   * A method that changes the size of the window depending on the size of the
   * downloaded level.
   */
  private void resize() {
    if (level == 0) {
      rows = Level.rowNumber * 25;
      columns = Level.columnNumber * 25;

      int currentH = (rows / 25 > 12 ? (rows / 25 - 12) * 25 + frameHeight
          : frameHeight);
      int currentW = (columns / 25 > 12 ? (columns / 25 - 12) * 25
          + frameWidth
          : frameWidth);
      setBounds();
      this.setSize(new Dimension(currentW, currentH));
    }
  }

  /**
   * A method that set default size of frame for default levels.
   */
  private void setDefaultSize() {
    rows = Level.rowNumber * 25;
    columns = Level.columnNumber * 25;
    setBounds();
    this.setSize(new Dimension(frameWidth, frameHeight));
  }

  /**
   * Getter for counter.
   */
  public JLabel getCounter() {
    return counter;
  }

  /**
   * Getter for GamePanel.
   */
  public GamePanel getGamePanel() {
    return gamePanel;
  }

}
