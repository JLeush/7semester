package view;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 * Class that load images.
 */
public class ImageUtilities {

  /**
   * Instance of this class.
   */
  private static ImageUtilities instance;

  /**
   * Method that get instance of this class.
   *
   * @return - instance.
   */
  public static ImageUtilities getInstance() {
    if (instance == null) {
      synchronized (ImageUtilities.class) {
        if (instance == null) {
          instance = new ImageUtilities();
        }
      }
    }
    return instance;
  }

  /**
   * Private constructor to prevent creating other instances of this class.
   */
  private ImageUtilities() {
  }

  private Map<String, Image> map = new HashMap<>();

  /**
   * Method that get image from resources.
   *
   * @param image - name of image.
   * @return Image or null if path is wrong.
   */
  public Image getImage(String image) {
    if (!map.containsKey(image)) {
      try {
        String path = getClass().getResource("/resources/" + image + ".jpg").getFile();
        Image img = (new ImageIcon(path)).getImage();
        map.put(image, img);
      } catch (NullPointerException npe) {
        Logger.getAnonymousLogger().log(Level.WARNING, "There is no such image in the resources");
        return null;
      }
    }
    return map.get(image);
  }
}
